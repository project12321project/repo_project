#include "User.h"
#include "Helper.h"

#define SUCCESS_CREAT_ROOM "1140"
#define FALIED_CREAT_ROOM "1141"

//C'tor
User::User(std::string userName, SOCKET sock)
{
	_userName = userName;
	_sock = sock;
}


//Output: user's name.
std::string User::getUserName()
{
	return _userName;
}

//Output: socket's number.
SOCKET User::getSocket()
{
	return _sock;
}

//Sends massege to user.
void User::send(string message)
{
	Helper::sendData(_sock, message);
}

//Output: Current room
Room* User::getRoom()
{
	return _currRoom;
}

//Output: Current game.
Game* User::getGame()
{
	return _currGame;
}

//Input: New to set.
void User::setGame(Game* game)
{
	_currGame = game;
}

//Initialized to null the game.
void User::clearGame()
{
	_currGame = nullptr;
}

//Initialized to null the room.
void User::clearRoom()
{
	_currGame = nullptr;
}


//Outpu:
//TRUE- Successed to join a room.
//FALSE- User already have a room or joining falied.
bool User::joinRoom(Room* newRoom)
{
	bool returnValue = false;
	if (!_currRoom) //No room.
	{
		if (_currRoom->joinRoom(this))
			returnValue = true;
	}
	return returnValue;
}


//In case the user has no room- creating new one.
//In case he has room - sending a falied message.
bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime)
{
	string s;
	if (!_currRoom) //No room.
	{
		s = SUCCESS_CREAT_ROOM;
		Room* newRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		this->send(s);
		return true;
	}
	else    //No need creating new room.
	{
		s = FALIED_CREAT_ROOM;
		this->send(s);
		return false;
	}
}

//Closes the game.
//Output: false- game closed. true- game is stil active.
bool User::leaveGame()
{
	bool returnValue = false;
	if (!_currGame)
	{
		if (_currGame->leaveGame(this))
			returnValue = true;
		_currGame = nullptr;
	}
	return returnValue;
}

//Closes the room.
//Output: Id if successed, -1 otherwise.
int User::closeRoom()
{
	int returnValue;
	if (returnValue = _currRoom->closeRoom(this))
	{
		_currRoom = nullptr;
	}
	return returnValue;
}

//Makes the user leave the room.
void User::leaveRoom()
{
	if (!_currRoom)
	{
		_currRoom->leaveRoom(this);
	}
	_currRoom = nullptr;
}

//get user is position
int User::getPosition()
{
	return _position;
}

bool User::isLoggedIn()
{
	return _loggedIn;
}

void User::disconnect()
{
	_loggedIn = false;
}


void User::setPosition(int position)
{
	_position = position;
}
