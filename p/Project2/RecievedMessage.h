#pragma once

#include <iostream>
#include "User.h"
#include "Helper.h"
#include <WinSock2.h>
#include <Windows.h>
#include <condition_variable>
#include <vector>
#include <string>


using namespace std;

class User;

class RecievedMessage
{
private:
	SOCKET _sock;
	MessageType _messageCode;
	User* _user;
	vector<string> _values;
	condition_variable _cond;

public:
	RecievedMessage(SOCKET sock, MessageType messagwCode);
	RecievedMessage(SOCKET sock, MessageType messageCode, vector<string> values);
	User* getUser();
	SOCKET getSock();
	void setUser(User*);
	MessageType getMessageCode();
	vector<string>& getValues();
	void releaseMessage();
	condition_variable* getCond();
};