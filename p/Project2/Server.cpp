#include "Server.h"
#include <sstream>


using namespace std;

condition_variable cond;
mutex lck;

Server::Server()
{
	Server::_roomIdSequence = 0;
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	ofstream data("data.txt", ios::app);
	data.close();

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		::closesocket(_serverSocket);
		for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
		{
			_connectedUsers.erase(it);
		}

		for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); it++)
		{
			_roomsList.erase(it);
		}
	}
	catch (...) {}
}

void Server::serve()
{
	bindAndListen();

	std::thread handleThread(&Server::handleRecievedMessages, this);
	handleThread.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}

void Server::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	int port = 8876;
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;
}


void Server::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	// the function that handle the conversation with the client
	std::thread handle(&Server::clientHandler, this, client_socket);
	handle.detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	try
	{
		int code;
		while (true)
		{
			code = Helper::getMessageTypeCode(clientSocket);
			if (code != 0)
			{
				addRecievedMessage(buildRecieveMessage(clientSocket, (MessageType)code));
			}
		}
		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}
}

void Server::addRecievedMessage(RecievedMessage* msg)
{
	_queRcvMessages.push(msg);
	std::lock_guard<std::mutex> lock(lck);
	lck.lock();
	cond.notify_all();
	waitToFinish(msg);
	lck.unlock();
}

RecievedMessage* Server::buildRecieveMessage(SOCKET client_socket, MessageType msgCode)
{
	return new RecievedMessage(client_socket, msgCode);
}

Room* Server::getRoomById(int roomId)
{
	for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); ++it)
	{
		Room* r = it->second;
		if (r->getId() == roomId)
		{
			return r;
		}
	}
	return nullptr;
}

User* Server::getUserByName(string username)
{
	for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		User* r = it->second;
		if (r->getUserName() == username)
		{
			return r;
		}
	}
	return nullptr;
}

User* Server::getUserBySocket(SOCKET client_socket)
{
	for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		User* r = it->second;
		if (r->getSocket() == client_socket)
		{
			return r;
		}
	}
	return nullptr;
}



void Server::handleRecievedMessages()
{
	while (true)
	{
		unique_lock<mutex> lock(lck);
		cond.wait(lock, [&]() {return !_queRcvMessages.empty(); });
		while (!_queRcvMessages.empty())
		{
			RecievedMessage* currentMessage = _queRcvMessages.front();
			currentMessage->setUser(getUserBySocket(currentMessage->getSock()));
			try {
				switch (currentMessage->getMessageCode())
				{
				case SIGH_IN:
					handleSignin(currentMessage);
					break;
				case SIGH_OUT:
					handleSignout(currentMessage);
					break;
				case SIGH_UP:
					handleSignup(currentMessage);
					break;
				case LIST_R_E:
					handleGetRooms(currentMessage);
					break;
				case LIST_R_ID:
					handleGetUsersInRoom(currentMessage);
					break;
				case JOIN_E_ROOM:
					handleJoinRoom(currentMessage);
					break;
				case LEAVE_ROOM:
					handleLeaveRoom(currentMessage);
					break;
				case CREATE_NEW_R:
					handleCreateRoom(currentMessage);
					break;
				case CLOSE_ROOM:
					handleCloseRoom(currentMessage);
					break;
				case START_GAME:
					handleStartGame(currentMessage);
					break;
				case ANSWER:
					handlePlayerAnswer(currentMessage);
					break;
				case QUIT_ROOM:
					break;
				case BEST_SCORES:
					handleGetBestScores(currentMessage);
					break;
				case STATUS:
					handleGetPersonalStatus(currentMessage);
					break;
				case QUIT_APLICTION:
					safeDeleteUesr(currentMessage);
					break;
				default:
					safeDeleteUesr(currentMessage);
					break;
				}
			}
			catch (const std::exception& e)
			{
				safeDeleteUesr(currentMessage);
			}
			currentMessage->releaseMessage();
			delete currentMessage;
			_queRcvMessages.pop();
		}
	}
}


void Server::safeDeleteUesr(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			if (u->getSocket() != NULL)
			{
				SOCKET s = u->getSocket();
				handleSignout(msg);
				closesocket(s);
			}
		}
	}
	catch (...) {}
}

User* Server::handleSignin(RecievedMessage* msg)
{
	try
	{
		string sendMsg = "102";
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			if (u->getSocket() != NULL)
			{
				SOCKET s = u->getSocket();
				int length = Helper::getIntPartFromSocket(s, 2);
				string name = Helper::getStringPartFromSocket(s, length);
				length = Helper::getIntPartFromSocket(s, 2);
				string password = Helper::getStringPartFromSocket(s, length);
				if (_db->isUserAndPassMatch(name, password))
				{
					if (getUserByName(name))
					{
						sendMsg += "2";
						u->send(sendMsg);
						return nullptr;
					}
					else
					{
						User* user = new User(name,s);
						if (_current == NULL)
						{
							_current = user;
							user->setPosition(1);
						}
						else
						{
							user->setPosition(_connectedUsers.size() + 2);
							_connectedUsers[s] = user;
						}
						sendMsg += "0";
						u->send(sendMsg);
						return user;
					}
				}
				else
				{
					sendMsg += "1";
					u->send(sendMsg);
					return nullptr;
				}
			}
		}
	}
	catch (...) { return nullptr; }
}

bool Server::handleSignup(RecievedMessage* msg)
{
	try
	{
		string sendMsg = "104";
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			if (u->getSocket() != NULL)
			{
				SOCKET s = u->getSocket();
				int length = Helper::getIntPartFromSocket(s, 2);
				string name = Helper::getStringPartFromSocket(s, length);
				length = Helper::getIntPartFromSocket(s, 2);
				string password = Helper::getStringPartFromSocket(s, length);
				length = Helper::getIntPartFromSocket(s, 2);
				string mail = Helper::getStringPartFromSocket(s, length);
				if (Validator::isPasswordValid(password))
				{
					if (Validator::isUsernameValid(name))
					{
						if(_db->isUserExists(name))
						{
							if(_db->addNewUser(name,password,mail))
							{
								sendMsg += "0";
								u->send(sendMsg);
								return true;
							}
							else
							{
								sendMsg += "4";
								u->send(sendMsg);
								return false;
							}
						}		
						else
						{
							sendMsg += "2";
							u->send(sendMsg);
							return false;
						}
					}	
					else
					{
						sendMsg += "3";
						u->send(sendMsg);
						return false;
					}	
				}
				else
				{
					sendMsg += "1";
					u->send(sendMsg);
					return false;
				}
			}
		}
	}
	catch (...) { return false; }
}


void Server::handleLeaveGame(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			Game* g = u->getGame();
			if (u->leaveGame())
			{
				g->~Game();
				delete(g);
			}
		}
	}
	catch (...) {}
}


void Server::handleStartGame(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			if (u->getRoom() != nullptr)
			{
				Room* r = u->getRoom();
				Game* game = new Game(r->getUsers(), r->getQuestionsNo(), *_db);
				std::map<int, Room*>::iterator it = _roomsList.find(r->getId());
				_roomsList.erase(it);
				game->sendFirstQuestion();
			}
			else
			{
				string s = "1180";
				u->send(s);
			}
		}
	}
	catch (...){}
}

void Server::handlePlayerAnswer(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			if (u->getGame() != nullptr)
			{
				Game* r = u->getGame();
				if (u->getSocket() != NULL)
				{
					SOCKET s = u->getSocket();
					int answer = Helper::getIntPartFromSocket(s, 2);
					int time = Helper::getIntPartFromSocket(s, 2);
					if (r->handleAnswerFromUser(u, answer, time))
					{
						delete r;
					}
				}
			}
		}
	}
	catch (...) {}
}

bool Server::handleCreateRoom(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			string sendMsg = "114";
			User* u = msg->getUser();
			if (u->getSocket() != NULL)
			{
				SOCKET s = u->getSocket();
				int length = Helper::getIntPartFromSocket(s, 2);
				string name = Helper::getStringPartFromSocket(s, length);
				char intPlayers = Helper::getStringPartFromSocket(s, 1)[0];
				int numQ = Helper::getIntPartFromSocket(s, 2);
				int timeA = Helper::getIntPartFromSocket(s, 2);
				_roomIdSequence++;
				if (u->createRoom(_roomIdSequence, name, intPlayers, numQ, timeA))
				{
					_roomsList[_roomIdSequence] = (Room*)u->getRoom();
					sendMsg += "0";
					u->send(sendMsg);
					return true;
				}
				else
				{
					sendMsg += "1";
					u->send(sendMsg);
					return false;
				}
			}
			else
			{
				sendMsg += "1";
				u->send(sendMsg);
				return false;
			}
		}
	}
	catch (...) { return false;}
}

bool Server::handleCloseRoom(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			if (u->getRoom() != nullptr)
			{
				Room* r = u->getRoom();
				if (u->closeRoom() != -1)
				{
					std::map<int, Room*>::iterator it = _roomsList.find(r->getId());
					for (size_t it = 0; it < r->getUsers().size(); it++)
						r->getUsers()[it]->send("116");
					_roomsList.erase(it);
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	catch (...) { return false; }
}

bool Server::handleJoinRoom(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			string sendMsg = "110";
			User* u = msg->getUser();
			if (u->getSocket() != NULL)
			{
				SOCKET s = u->getSocket();
				int idR = Helper::getIntPartFromSocket(s, 4);
				if (getRoomById(idR) != nullptr)
				{
					Room* r = getRoomById(idR);
					u->joinRoom(r);
				}
				else
				{
					sendMsg += "2";
					u->send(sendMsg);
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	catch (...) { return false; }
}

bool Server::handleLeaveRoom(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			if (u->getRoom() != nullptr)
			{
				Room* r = u->getRoom();
				u->leaveRoom();
				u->send("1120");
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	catch (...) { return false; }
}

void Server::handleGetUsersInRoom(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			string sendMsg = "108";
			User* u = msg->getUser();
			if (u->getSocket() != NULL)
			{
				SOCKET s = u->getSocket();
				int idR = Helper::getIntPartFromSocket(s, 4);
				if (getRoomById(idR) != nullptr)
				{
					Room* r = getRoomById(idR);
					string list = "";
					for (size_t it = 0; it < r->getUsers().size(); it++)
					{
						std::string name = r->getUsers()[it]->getUserName();
						size_t len = name.length();
						stringstream ss;
						ss << len;
						list += ss.str();
						list += r->getUsers()[it]->getUserName();
					}
					sendMsg += list;
					u->send(sendMsg);
				}
				else
				{
					sendMsg += "0";
					u->send(sendMsg);
				}
			}
		}
	}
	catch (...) {}
}


void Server::handleGetRooms(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			string s = "106";
			for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); ++it)
			{
				if (it->second != nullptr)
				{
					Room* r = it->second;
					s += r->getId();
					string name = r->getName();
					size_t len = name.length();
					stringstream ss;
					ss << len;
					s += ss.str();
					s += name;
				}
			}
			u->send(s);
		}
	}
	catch (...) {}
}


void Server::handleGetBestScores(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			std::string sendMsg = "124";
			vector<string> best = _db->getBestScores();
			for (std::vector<string>::iterator it = best.begin(); it != best.end(); ++it)
			{
				if (it->data() != nullptr)
				{
					std::string name = it->data();
					size_t len = name.length();
					stringstream ss;
					ss << len;
					sendMsg += ss.str();
					sendMsg += name;
				}
			}
			u->send(sendMsg);
		}
	}
	catch (...) {}
}

void Server::handleGetPersonalStatus(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			User* u = msg->getUser();
			string sendMsg = "126";
			vector<string> best = _db->getPersonalStatus(u->getUserName());
			for (std::vector<string>::iterator it = best.begin(); it != best.end(); ++it)
			{
				if (it->data() != nullptr)
				{
					std::string name = it->data();
					size_t len = name.length();
					stringstream ss;
					ss << len;
					sendMsg += ss.str();
					sendMsg += name;
				}
			}
			u->send(sendMsg);
		}
	}
	catch (...) {}
}

void Server::handleSignout(RecievedMessage* msg)
{
	if (msg->getUser() != nullptr)
	{
		User* user = msg->getUser();
		if (msg->getUser() == _current)
		{
			User* tmpUser = _current;
			tmpUser->leaveRoom();
		}
		else
		{
			queue<User*> tmp;
			int ind = 2;
			for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
			{
				User* r = it->second;
				r->setPosition(ind);
				ind++;
			}
			delete user;
		}
	}
}

void Server::waitToFinish(RecievedMessage* msg)
{
	mutex mtx;
	unique_lock<mutex> waitLcker(mtx);
	msg->getCond()->wait(waitLcker);
}