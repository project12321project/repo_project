
#include "Question.h"


//C'tor.	
//Intialized _answers in random way.
Question::Question(int id, string question, string correctAns, string ans2, string ans3, string ans4) : _id(id), _question(question)
{
	int rand;
	std::vector<string> vec{ correctAns, ans2, ans3, ans4 };
	for (int i = ANSWERS_NUM; i > 0; i--)
	{
		rand = std::rand() % i;
		if (!rand)   //The position of the correct answer.
			_correctAnswerIndex = rand;
		_answers[ANSWERS_NUM - i - 1] = vec[rand];

		vec.erase(vec.begin() + rand - 1);  //Erases the element that was already copied.
	}
}

//Output: The arr of answers.
string* Question::getAnswers()
{
	return(_answers);
}

//Output: The question.
string Question::getQuestion()
{
	return(_question);
}

//Output: The index of the correct answer.
int Question::getCorrectAnswerIndex()
{
	return(_correctAnswerIndex);
}

//Output: id of question.
int Question::getId()
{
	return (_id);
}

//D'tor.
Question::~Question()
{
}