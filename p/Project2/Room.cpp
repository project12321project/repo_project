#include "Room.h"
#include "User.h"

#define JOIN_FAILED "1102"
#define FULL_ROOM "1101"
#define USERS_IN_ROOM "108"
#define JOIN_SUCCESSED "1100"
#define CLOS_ROOM "116"
#define LEAV_ROOM "112"
#define CONNECTED_USERS "207"


//C'tor
Room::Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionsNo)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionsNo = questionsNo;
	_questionTime = questionTime;
	_users.push_back(_admin);
}


//Output: Vector of users.
vector<User*> Room::getUsers()
{
	return _users;
}

//Output: room questionNo.
int Room::getQuestionsNo()
{
	return _questionsNo;
}

//Output: room id.
int Room::getId()
{
	return _id;
}

//Output: room name.
string Room::getName()
{
	return _name;
}

//send STRING to USER.
void Room::sendMessage(User* us, string st)
{	
	for (size_t i = 0; i < _users.size(); i++)
	{
		if (us != _users.at(i))
		{
			(_users.at(i))->send(st);     //Sends message to all users excapt the giving one.
		}
	}
}

//Input: send a STRING to all connected users.
void Room::sendMessage(string st)
{
	sendMessage(nullptr, st);
}

//Input: new user to add the room. 
//Output: true if new user were added.
bool Room::joinRoom(User* newUser)
{
	bool flag = true;
	string s;
	if (_maxUsers > (int)_users.size() + 1)
	{
		s = JOIN_SUCCESSED;   //Overload, Convert.
		_users.push_back(newUser);
		Room::sendMessage(newUser, s);
		sendMessage(getUsersListMessage());
	}
	else   //Join Falid .
	{
		s = FULL_ROOM;  //Overload, Convert.
		flag = false;
		sendMessage(newUser, s);
	}
	return flag;
}



//Input: user to remove from the room.
void Room::leaveRoom(User* userToRemove)
{
	string s = LEAV_ROOM;
	_users.erase(std::remove(_users.begin(), _users.end(), userToRemove), _users.end());
	sendMessage(userToRemove, s);
	sendMessage(getUsersListMessage());
}



//Buliding the protocol according the given file.
//108 numberOfUsers ## username ## username  or 1080 if the room doesnot exists. 
string Room::getUsersListMessage()
{
	string returnedValue = USERS_IN_ROOM;
	string name;
	if (this)
	{
		for (size_t i = 0; i < _users.size(); i++)
		{
			name = _users.at(i)->getUserName();
			returnedValue += _users.size() + "##" + (string)(name.length() + "##" + name);
		}
	}
	else
		returnedValue += "0";
	return returnedValue;
}

//Output: -1 if closed falied, id otherwise.
int Room::closeRoom(User* user)
{
	int returnValue = -1;
	if (user == _admin && user->closeRoom() != -1)
	{
		for (size_t i = 0; i < _users.size(); i++)
		{
			_users.at(i)->send(CLOS_ROOM);
			if (_users.at(i) != _admin)
				_users.at(i)->clearRoom();
		}
		returnValue = _id;
	}
	return returnValue;
}



