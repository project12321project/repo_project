#include "Validator.h"


Validator::Validator() {

}

Validator::~Validator() {}

bool Validator::isPasswordValid(string password)
{
	int _check[SIZE];
	if (password.length() >= 4)
	{
		if (vaildCharacters(password,true))
		{
			if (_check[0] > 0 && _check[1] > 0 && _check[2] > 0)
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool Validator::vaildCharacters(string password, bool b)
{
	int _check[SIZE];
	for (int i = 0; i < 3; i++)
	{
		_check[i] = 0;
	}

	for (size_t i = 0; i < password.length(); i++)
	{
		if (password[i] == ' ')
		{
			return false;
		}
		else if(b)
		{
			if (isDigit(password[i]))
			{
				_check[0]++;
			}
			else if (isSmallLetter(password[i]))
			{
				_check[1]++;
			}
			else if (isBigLetter(password[i]))
			{
				_check[2]++;
			}
		}
	}
	return true;
}

bool Validator::isDigit(char c)
{
	return (c >= 48 && c <= 57);
}

bool Validator::isSmallLetter(char c)
{
	return(c >= 97 && c <= 122);
}

bool Validator::isBigLetter(char c)
{
	return (c >= 65 && c <= 90);
}



bool Validator::isUsernameValid(string userName)
{
	if (userName.length() > 0)
	{
		if (isBigLetter(userName[0]) || isSmallLetter(userName[0]))
		{
			return (vaildCharacters(userName, false));
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}
