#include "DataBase.h"

//C'tor.
DataBase::DataBase()
{

	try
	{
		sqlite3_open("sqlite3.cpp", ppDb);
	}
	catch (string s)
	{
		throw s;
	}
}

//D'tor.
DataBase::~DataBase()
{
	sqlite3_close(*ppDb);
}

//Insert new field to users table.
bool DataBase::addNewUser(string userName, string pass, string email)
{
	bool returnValue = true;
	std::string q = ("insert into t_users VALUES(" + userName + pass + email +")");
	const char * que = q.c_str();
	if (sqlite3_exec(*ppDb, que, NULL, NULL, NULL))
		returnValue = false;
	return returnValue;
}

//Update a field in gamnes table by given game_id. Update the status to end-1 and the end_game_time.
bool DataBase::updateGamesStatus(int id)
{
	bool returnValue = true;
	time_t  timev;
	time(&timev);
	std::string q = ("update t_gemes set status = 1 and end_time = " + std::to_string(timev) + "where game_id = " + std::to_string(id));
	const char* que = q.c_str();
	if (sqlite3_exec(*ppDb, que, NULL, NULL, NULL))
		returnValue = false;
	return returnValue;
}

//Insert field to players answers table.
bool DataBase::addAnswerToPlayer(int gameId, string userName, int questionId, string answer, bool isCorrect, int answerTime)
{
	bool returnValue = true;
	std::string q = ("insert t_players_answers VALUES(" + std::to_string(gameId) + userName + std::to_string(questionId) + answer + std::to_string(isCorrect) + std::to_string(answerTime) + ")");
	const char* que = q.c_str();
	if (sqlite3_exec(*ppDb, que, NULL, NULL, NULL))
		returnValue = false;
	return returnValue;
}

//Checks if given user exists in users table.
bool DataBase::isUserExists(string userName)
{
	bool found;
	std::string q = "select * from t_users where username =" + userName;
	const char* que = q.c_str();
	struct sqlite3_stmt *selectstmt;
	int result = sqlite3_prepare_v2(*ppDb, que, -1, &selectstmt, NULL);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(selectstmt) == SQLITE_ROW)
		{
			found = true;	// record found
		}
		else
		{
			found = false;	// no record found
		}
	}
	sqlite3_finalize(selectstmt);
	return found;
}

//Checks if given user and pass exists at the sme field in users table.
bool DataBase::isUserAndPassMatch(string user, string pass)
{
	bool found;
	std::string q = "select * from t_users where username = " + user + "and password = " + pass;
	const char* que = q.c_str();
	struct sqlite3_stmt *selectstmt;
	int result = sqlite3_prepare_v2(*ppDb, que, -1, &selectstmt, NULL);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(selectstmt) == SQLITE_ROW)
		{
			found = true;	// record found
		}
		else
		{
			found = false;	// no record found
		}
	}
	sqlite3_finalize(selectstmt);
	return found;
}

std::vector < std::string > v;
std::vector <Question*> queVec;
int counter = 0;

//Insert new field in the game table.
//Output: the game_id.
int DataBase::insertNewGame()
{
	time_t  timev;
	time(&timev);
	int game = 0;
	sqlite3_exec(*ppDb, "select * from t_games", callbackCount, NULL, NULL);
	counter++;
	string q = "insert into t_gemes VALUES(" + to_string(counter) + to_string(game) + to_string(timev) + to_string(game) + ")";
	const char* que = q.c_str();
	sqlite3_exec(*ppDb, que, NULL, NULL, NULL);
	return counter;
}


//Output: vector of questions. The number of questions is given.
vector<Question*> DataBase::initQuestions(int quesNum)
{
	string q = "SELECT * question from t_questions LIMIT " + quesNum;
	const char* que = q.c_str();
	sqlite3_exec(*ppDb, que, callbackQuestions, NULL, NULL);
	return queVec;
}


//Creates vector of questions.
int DataBase::callbackQuestions(void* data, int argc, char** argv, char**)
{
	/*std::vector <int> aa;
	for (int i = 0; i < argc; i++)
		aa.push_back(i);*/

	Question que((int)argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
	queVec.push_back(&que);
	return 0;
}


//Return the fields number;
int DataBase::callbackCount(void* unused, int argc, char** data, char** columes)
{
	counter = argc;
	return 0;
}




int DataBase::callbackPersonalStatus(void*, int, char**, char**){
	return 1;
}



vector<string> DataBase::getBestScores() {

	std::vector <string> aa;
	for (int i = 0; i < 5; i++)
	aa.push_back(to_string(i));
	return aa;
}



int DataBase::callbackBestScores(void*, int, char**, char**){
	return 1;
}



vector<string> DataBase::getPersonalStatus(string userName)
{
	/*char* que;
	if (&userName != nullptr)
	que = "select * game_id from t_players_answers where username=%s union select* status from t_games", userName;
	else
	que = "select * from ";
	sqlite3_exec(*ppDb, que, callbackPersonalStatus, NULL, NULL);*/
	std::vector <string> aa;
	for (int i = 0; i < 5; i++)
		aa.push_back(to_string(i));
	return aa;
}