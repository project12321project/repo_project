#pragma once
#include <iostream>
#include "User.h"
#include "Helper.h"
#include "RecievedMessage.h"
//#include <WinSock2.h>
#include <Windows.h>
#include <condition_variable>
#include <algorithm>
#include <map>
#include "Server.h"
#include "DataBase.h"

#define SIZE 3

using namespace std;

class Validator
{
private:
	static bool vaildCharacters(string password,bool b);
	static bool isDigit(char c);
	static bool isSmallLetter(char c);
	static bool isBigLetter(char c);

public:
	Validator();
	~Validator();
	static bool isPasswordValid(string password);
	static bool isUsernameValid(string userName);
	
};

//int Validator::_check[] = {NULL, NULL, NULL};