#pragma once

#include "User.h"
#include "Helper.h"
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class User;

class Room
{
private:
	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;

	string getUserAsString(vector<User*>, User*);
	void sendMessage(string st);
	void sendMessage(User* us, string st);

public:
	Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionsNo);
	bool joinRoom(User* newUser);
	int closeRoom(User* user); 
	void leaveRoom(User* userToRemove);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo(); 
	int getId();
	string getName();

};
