#include "Game.h"
#include "User.h"
#include "DataBase.h"


Game::Game(const vector<User*>& players, int questionsNo, DataBase& db)
{
	_players = players;
	//db->copyConstractor
	try
	{
		//DataBase::insertNewGame();
		//DataBase::initQuestions(questionsNo);
		for (size_t i = 0; i < players.size(); i++)
		{
			players[i]->setGame(this);
		}

	}
	catch (...) {}
}

Game::~Game()
{
	try
	{
		for (std::vector<User*>::iterator it = _players.begin(); it != _players.end(); ++it)
			_players.erase(it);

		for (std::vector<string>::iterator it = _questions.begin(); it != _questions.end(); ++it)
			_questions.erase(it);

	}
	catch (...) {}
}

void Game::sendQuestionToAllUsers()
{
	string msg = "118";
	for (std::vector<string>::iterator it = _questions.begin(); it != _questions.end(); ++it)
		msg += *it;
	_currentTurnAnswers = 0;
	for (size_t i = 0; i < _players.size(); i++)
	{
		try
		{
			_players[i]->send(msg);
		}
		catch (...) {}
	}
}

void Game::handleFinishGame()
{
	//DataBase::updateGameStatus
	for (size_t i = 0; i < _players.size(); i++)
	{
		try
		{
			_players[i]->setGame(nullptr);
		}
		catch (...) {}
	}
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

bool Game::handleNextTurn()
{
	bool returnValue = false;
	if (!_players.empty())
	{
		//checks
		sendQuestionToAllUsers();
		returnValue = true;
	}
	else
	{
		handleFinishGame();
	}
	return returnValue;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	_currentTurnAnswers++;
	string msg;
	//check
	if (answerNo == 5)
	{
		//send 120 msg empty
		handleNextTurn();
		return false;
	}
	else
	{
		//DataBase::addAnswerToPlayer();
		//send 120 msg 
		return true;
	}

}

bool Game::leaveGame(User* currUser)
{
	for (std::vector<User*>::iterator it = _players.begin(); it != _players.end(); ++it)
	{
		if (*it == currUser)
		{
			_players.erase(it);
			handleNextTurn();
			return true;
		}
	}
	handleNextTurn();
	return false;
}