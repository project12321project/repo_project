#pragma once

#define WIN32_LEAN_AND_MEAN
#include <iostream>
#include "User.h"
#include "Helper.h"
#include "RecievedMessage.h"
#include <WinSock2.h>
#include <Windows.h>
#include <condition_variable>
#include <algorithm>
#include <map>
#include "DataBase.h"

using namespace std;

class DataBase;

class Game
{
private:
	std::map<string, int> _results;
	int _currentTurnAnswers;
	vector<User*> _players;
	vector<string> _questions;

public:
	Game(const vector<User*>& players, int questionsNo, DataBase& db);
	~Game();
	void sendQuestionToAllUsers();
	void handleFinishGame();
	void sendFirstQuestion();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser);
};

