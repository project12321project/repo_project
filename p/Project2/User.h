#pragma once
#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include <socketapi.h>

#include "Room.h"
#include "Game.h"

using namespace std;
class Helper;
class Room;
class Game;

class User
{
private:
	std::string _userName;
	Room* _currRoom;
	Game* _currGame;
	int _position;
	bool _loggedIn;
	SOCKET _sock;

public:
	User(std::string name, SOCKET socket);
	void send(std::string message);
	std::string getUserName();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* game);
	void clearGame();
	void clearRoom();
	bool createRoom(int, std::string, int, int, int);
	int getPosition();
	bool isLoggedIn();
	void disconnect();
	void setPosition(int position);

	bool joinRoom(Room*);
	bool leaveGame();
	int closeRoom();
	void leaveRoom();
	
}; 
