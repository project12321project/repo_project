#include "RecievedMessage.h"

//C' tor
RecievedMessage::RecievedMessage(SOCKET sock, MessageType messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
}

//C'tor
RecievedMessage::RecievedMessage(SOCKET sock, MessageType messageCode, vector<string> values)
{
	_sock = sock;
	_messageCode = messageCode;
}

//Output: The user.
User * RecievedMessage::getUser()
{
	return _user;
}

//Outpur: Socket of message.
SOCKET RecievedMessage::getSock()
{
	return _sock;
}

//Output: The code message.
MessageType RecievedMessage::getMessageCode()
{
	return _messageCode;
}

//Output: Vector of strings.
vector<string>& RecievedMessage::getValues()
{
	return _values;
}

//Input: new user to set.
void RecievedMessage::setUser(User* user)
{
	_user = user;
}


condition_variable * RecievedMessage::getCond()
{
	return &_cond;
}

void RecievedMessage::releaseMessage()
{
	_cond.notify_all();
}