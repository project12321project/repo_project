#pragma once

#include <string>
#include <random>
#include <iostream>
#include <vector>

#define ANSWERS_NUM 4

using namespace std;


class Question
{
public:
	Question(int id, string question, string correctAns, string ans2, string ans3, string ans4);
	~Question();
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	string _question;
	string _answers[ANSWERS_NUM];
	int _correctAnswerIndex;
	int _id;
};


