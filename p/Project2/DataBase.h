#pragma once

#define WIN32_LEAN_AND_MEAN
#include <iostream>
#include <string>
#include <Windows.h>
#include <vector>
#include <algorithm>
#include "sqlite3.h"
#include "Server.h"
#include <ctime>
#include "Question.h"
#include <time.h>
#include "Validator.h"


using namespace std;

class sqlite3;
class Server;
class question;
class Validator;

class DataBase
{
private:
	sqlite3 **ppDb;
	static int callbackCount(void*, int, char**, char**);
	static int callbackQuestions(void*, int, char**, char**);
	static int callbackBestScores(void*, int, char**, char**);
	static int callbackPersonalStatus(void*, int, char**, char**);

public:
	DataBase();
	~DataBase();
	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string user, string pass);
	vector<Question*> initQuestions(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGamesStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);
};