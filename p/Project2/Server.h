#pragma once

#define NOMINMAX
#include "RecievedMessage.h"
#include "Game.h"
#include "Validator.h"
#include "DataBase.h"
#include "Helper.h"
#include "User.h"
#include "Room.h"
#include <exception>
#include <mutex>
#include <vector>
#include <condition_variable>
#include <fstream>
#include <thread>
#include <iostream>
#include <string>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
//#include <WinSock2.h>
//#include <winsock.h >
#include <queue>
#include <map>

using namespace std;

class DataBase;
class Room;
class User;
class RecievedMessage;
class Validator;
class Game;
class Helper;
class Question;
class sqlite3;

class Server
{
public:
	Server();
	~Server();
	void serve();
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET clientSocket);
	Room* getRoomById(int roomId);
	User* getUserByName(string username);
	User* getUserBySocket(SOCKET client_socket);
	void handleRecievedMessages();
	void safeDeleteUesr(RecievedMessage* msg);
	User* handleSignin(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);
	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);
	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);
	void addRecievedMessage(RecievedMessage* msg);
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, MessageType msgCode);
	void waitToFinish(RecievedMessage* msg);
	

private:
	std::map<SOCKET, User*> _connectedUsers;
	std::map<int, Room*> _roomsList;
	int _roomIdSequence;
	std::queue<RecievedMessage*> _queRcvMessages;
	SOCKET _serverSocket;
	User* _current;
	DataBase* _db;
};

//int Server::_roomIdSequence = 0; //initialization

void ret(queue<User*>& from, queue<User*>& to);
